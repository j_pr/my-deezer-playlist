import { Component, isDevMode, OnDestroy, OnInit, ViewChild } from "@angular/core";
import { DeezerServices } from "./services/deezer-services.service";
import { Playlist } from "./models/playlist";
import { MatSort, MatTableDataSource } from "@angular/material";
import { MyLoadingService } from "../utilities/my-loading/service/my-loading.service";
import { Router } from "@angular/router";
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';

@Component({
  selector: "app-playlist",
  templateUrl: "./playlist.component.html",
  styleUrls: ["./playlist.component.css"]
})
export class PlaylistComponent implements OnInit, OnDestroy {
  errorMessage: string;
  playlists: Playlist[];

  displayedColumns: string[] = ["picture_medium", "title", "duration"];
  dataSource = new MatTableDataSource<Playlist>();
  @ViewChild(MatSort) sort: MatSort;

  constructor(
    private deezerHTTP: DeezerServices,
    private load: MyLoadingService,
    private router: Router
  ) {}

  ngOnInit() {
    // initialisation loading page
    this.load.loadingOn();
    if (isDevMode()) {
      console.log("initialisation component PlaylistComponent");
    }
    // get list playlist
    this.initPlaylists()
  }

  ngOnDestroy(){
    this.playlists = undefined;
  }

  initPlaylists(){
    this.playlists = [];
    this.deezerHTTP.getUserPlaylist(5).subscribe((res: HttpResponse<Playlist[]>) => {
      this.playlists = this.makePlaylists(res.body);
      // sort playlists by title
      this.sortPlaylists();
      // conf table component
      this.setTableData();
      this.load.loadingOff();
    },
      (res: HttpErrorResponse) => {
      this.errorMessage = <any>res.message;
      this.load.loadingOff();
      this.router.navigate(['**']);
      }
    );
  }

  makePlaylists(res: any) {
    const listPlaylist: Playlist[] = [];
    if (res.data !== null && res.data !== undefined) {
      res.data.forEach(element => {
        listPlaylist.push(new Playlist(element));
      });
    }
    return listPlaylist;
  }

  applyFilter(filterValue: string): void {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  setTableData(): void {
    this.dataSource = new MatTableDataSource(this.playlists);
    this.dataSource.sortingDataAccessor = (
      data: any,
      sortHeaderId: string
    ): string => {
      if (typeof data[sortHeaderId] === "string") {
        return data[sortHeaderId].toLocaleLowerCase();
      }
      return data[sortHeaderId];
    };
    this.dataSource.sort = this.sort;
  }

  sortPlaylists() {
    this.playlists.sort((a: Playlist, b: Playlist) =>
      this.sortString(a.title, b.title)
    );
  }

  sortString(a: string, b: string) {
    const textA = a.toUpperCase();
    const textB = b.toUpperCase();
    return textA.localeCompare(textB);
  }

  onClickPlaylist(id: number) {
    this.router.navigate(['/playlist',id,'details']);
  }
}
