export class Playlist {
  id: number;
  picture_medium: string;
  title: string;
  duration: number;
  creator: string;

  constructor(o: any) {
    if (o !== null && o !== undefined) {
      this.id = o.id;
      this.picture_medium = o.picture_medium;
      this.title = o.title;
      this.duration = o.duration;
      this.creator = o.creator.name;
    }
  }
}
