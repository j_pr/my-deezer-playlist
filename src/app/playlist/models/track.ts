export class Track {
  id: number;
  title: string;
  artist: string;
  duration: string;

  constructor(o: any) {
    if (o !== null && o !== undefined) {
      this.id = o.id;
      this.title = o.title;
      this.artist = o.artist.name;
      this.duration = o.duration;
    }
  }
}
