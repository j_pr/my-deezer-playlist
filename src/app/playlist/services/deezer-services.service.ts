import { Injectable, isDevMode } from "@angular/core";
import { Observable, throwError } from "rxjs";
import { HttpClient, HttpErrorResponse, HttpResponse } from "@angular/common/http";
import { Playlist } from "../models/playlist";
import { Track } from "../models/track";
import { environment } from "../../../environments/environment";
import { catchError, tap } from 'rxjs/operators';

type EntityResponsePlaylist = HttpResponse<Playlist[]>;
type EntityResponseHeader = HttpResponse<Playlist>;
type EntityResponseTracks = HttpResponse<Track[]>;

@Injectable({
  providedIn: "root"
})
export class DeezerServices {
  constructor(private http: HttpClient) {}

  getUserPlaylist(userId: number): Observable<EntityResponsePlaylist> {
    let url = environment.rest.corsProxy + "/" + environment.rest.deezer.urlUserPlaylist;
    url = url.replace(":userId", userId.toString());
    if (isDevMode()) {
      console.log("service call getUserPlalist");
    }
    return this.http.get<Playlist[]>(url, {observe: 'response'}).pipe(
      tap(data => console.log("getUserPlalist : ", data)),
      catchError(this.handleError)
    );
  }

  getPlaylistInformation(playlistId: number): Observable<EntityResponseHeader> {
    let url = environment.rest.corsProxy + "/" + environment.rest.deezer.urlPlaylistInformation;
    if (playlistId !== undefined){
      url = url.replace(":playlistId", playlistId.toString());
    }
    if (isDevMode()) {
      console.log("service call getPlaylistInformation");
    }
    return this.http.get<Playlist>(url, {observe: 'response'}).pipe(
      tap(data => console.log("getPlaylistInformation : ", data)),
      catchError(this.handleError)
    );
  }

  getTracksInformation(playlistId: number): Observable<EntityResponseTracks> {
    let url = environment.rest.corsProxy + "/" + environment.rest.deezer.urlTracksInformation;
    if (playlistId !== undefined) {
      url = url.replace(":playlistId", playlistId.toString());
    }
    if (isDevMode()) {
      console.log("service call getTracksInformation");
    }
    return this.http.get<Track[]>(url, {observe: 'response'}).pipe(
      tap(data => console.log("getTracksInformation : ", data)),
      catchError(this.handleError)
    );
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error("An error occurred:", error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` + `body was: ${error.error}`
      );
    }
    // return an observable with a user-facing error message
    return throwError("Something bad happened; please try again later.");
  }
}
