import { TestBed } from "@angular/core/testing";

import { DeezerServices } from "./deezer-services.service";
import { HttpClientModule} from "@angular/common/http";
import { of } from "rxjs";

describe("DeezerServices TEST", () => {
  let deezerService: DeezerServices;
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule],
      providers: [DeezerServices]
    });
    deezerService = TestBed.get(DeezerServices);
  });

  it("should be created : DeezerServices", () => {
    expect(deezerService).toBeTruthy();
  });

  describe("getUserPlaylist", () => {
    it("should return a collection of playlist", () => {
      const playlists = [
        {
          id: "1",
          picture_medium: "picture1",
          title: "title1",
          duration: "100",
          creator: "creator1"
        },
        {
          id: "2",
          picture_medium: "picture2",
          title: "title2",
          duration: "200",
          creator: "creator2"
        }
      ];
      let response;
      spyOn(deezerService, "getUserPlaylist").and.returnValue(of(playlists));

      deezerService.getUserPlaylist(5).subscribe(res => {
        response = res;
      });

      expect(response).toEqual(playlists);
    });
  });

  describe("getPlaylistInformation", () => {
    it("should return a Header", () => {
      const header = {
          id: "1",
          picture_medium: "picture1",
          title: "title1",
          duration: "100",
          creator: "creator1"
        };
      let response;
      spyOn(deezerService, "getPlaylistInformation").and.returnValue(of(header));

      deezerService.getPlaylistInformation(5).subscribe(res => {
        response = res;
      });

      expect(response).toEqual(header);
    });
  });

  describe("getTracksInformation", () => {
    it("should return a collection of track", () => {
      const tracks = [
        {
          id: "1",
          title: "title1",
          artist: "artist1",
          duration: "100"
        },
        {
          id: "2",
          title: "title2",
          artist: "artist2",
          duration: "200"
        }
      ];
      let response;
      spyOn(deezerService, "getTracksInformation").and.returnValue(of(tracks));

      deezerService.getTracksInformation(5).subscribe(res => {
        response = res;
      });

      expect(response).toEqual(tracks);
    });
  });
});
