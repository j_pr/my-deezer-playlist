import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { PlaylistComponent } from "./playlist.component";
import { NO_ERRORS_SCHEMA } from "@angular/core";
import { SecondToTimePipe } from "../utilities/TimePipe";
import { MatTableModule } from "@angular/material";
import { HttpClientModule } from "@angular/common/http";
import { RouterTestingModule } from "@angular/router/testing";

describe("PlaylistComponent TEST", () => {
  let component: PlaylistComponent;
  let fixture: ComponentFixture<PlaylistComponent>;
  let playlistsMock = [
    {
      id: 2,
      picture_medium: "picture2",
      title: "title2",
      duration: 200,
      creator: "creator2"
    },
    {
      id: 1,
      picture_medium: "picture1",
      title: "title1",
      duration: 100,
      creator: "creator1"
    }
  ];

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PlaylistComponent, SecondToTimePipe],
      imports: [MatTableModule, HttpClientModule, RouterTestingModule],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlaylistComponent);
    component = fixture.componentInstance;

    fixture.detectChanges();
    console.log('beforeEach : ', component);
  });

  it("should create PlaylistComponent", () => {
    expect(component).toBeTruthy();
  });

  it("component with playlists", () => {
    component.playlists = playlistsMock;
    expect(component.playlists[1].id).toEqual(1);
  });

  it("sortPlaylist()", () => {
    component.playlists = playlistsMock;
    component.sortPlaylists();
    expect(component.playlists[1].id).toEqual(2);
  });

});
