import { Component, isDevMode, OnDestroy, OnInit } from "@angular/core";
import { ActivatedRoute, Router} from "@angular/router";
import { DeezerServices } from "../services/deezer-services.service";
import { MyLoadingService } from "../../utilities/my-loading/service/my-loading.service";
import { Playlist} from "../models/playlist";
import { Track} from "../models/track";
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';

@Component({
  selector: "app-playlist-details",
  templateUrl: "./playlist-details.component.html",
  styleUrls: ["./playlist-details.component.css"]
})
export class PlaylistDetailsComponent implements OnInit, OnDestroy {
  errorMessage: string;
  playlistId: number;
  playlist: Playlist;
  tracks: Track[];

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private deezerHTTP: DeezerServices,
    private load: MyLoadingService
  ) {
    // get playlistId in url params
    this.route.params.subscribe(params => {
      this.playlistId = params.id;
    });
  }

  ngOnInit() {
    this.load.loadingOn();
    if (isDevMode()) {
      console.log('initialisation component PlaylistDetailsComponent');
    }
    // get Header infos to display
    this.initPlaylist();
    // get tracks infos to display
    this.initTracks();
  }

  ngOnDestroy(){
    this.playlist = undefined;
    this.tracks = undefined;
  }

  initPlaylist(){
    this.playlist = new Playlist(undefined);
    this.deezerHTTP.getPlaylistInformation(this.playlistId).subscribe(
      (res: HttpResponse<Playlist>)  => {
        this.playlist = new Playlist(res.body);
      },
      (res: HttpErrorResponse) => {
        this.errorMessage = <any>res.message;
      }
    );
  }

  initTracks(){
    this.tracks = [];
    this.deezerHTTP.getTracksInformation(this.playlistId).subscribe(
      (res: HttpResponse<Track[]>)  => {
        this.tracks = this.makeTracks(res.body);
        this.load.loadingOff();
      },
      (res: HttpErrorResponse) => {
        this.errorMessage = <any>res.message;
        this.load.loadingOff();
      }
    );
  }

  makeTracks(res: any) {
    if (res.error) {
      console.warn('no data for playlistId : ', this.playlistId,'. Error : ', res.error);
      this.load.loadingOff();
      this.router.navigate(['**']);
    }
    const listTracks: Track[] = [];
    if (res.data !== null && res.data !== undefined) {
      res.data.forEach(element => {
        listTracks.push(new Track(element));
      });
    }
    return listTracks;
  }

  emailJuriaan(res: string) {
    let url: string;
    const retourLigne = '%0D%0A';
    const destinataires = 'juriaanponcelin@gmail.com';
    const objet = 'projet betclic';
    const bodyOK = 	'Bonjour,' + retourLigne + retourLigne +
      'Je t\'informe avec plaisir que la qualité de ton projet te permet, si tu le souhaites, de continuer notre processus de recrutement.' + retourLigne + retourLigne +
      'Nous aimerions donc te revoir.' + retourLigne +
      'Merci de revenir vers nous  pour que nous puissions convenir d\'une date pour rencontrer l\'équipe.'  + retourLigne + retourLigne +
      'Je te souhaite une agréable fin de journée, et à bientôt j\'espère.' + retourLigne +
      'Cordialement,' + retourLigne +
      'Ta future équipe.';

    const bodyKO = 	'Bonjour,' + retourLigne + retourLigne +
      'Malgré vos qualités évidentes, nous sommes au regret de vous informer que votre candidature n\'a pas été retenue.' + retourLigne + retourLigne +
      'Nous nous permettons toutefois, sauf avis contraire de votre part, de conserver votre curriculum vitae.' + retourLigne + retourLigne +
      'Nous vous souhaitons une pleine réussite dans votre recherche.' + retourLigne +
      'Nous vous prions d\'agréer, Monsieur, l\'expression de nos respectueuses salutations.';

    if (res === 'ok') {
      url = 'mailto:' + destinataires + '?subject=' + objet + '&body=' + bodyOK;
    } else {
      url = 'mailto:' + destinataires + '?subject=' + objet + '&body=' + bodyKO;
    }
    location['href'] = url;
  }
}
