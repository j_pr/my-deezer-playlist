import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { PlaylistHeaderComponent } from "./playlist-header.component";
import { SecondToTimePipe } from "../../../utilities/TimePipe";
import { NO_ERRORS_SCHEMA } from "@angular/core";

describe("PlaylistHeaderComponent TEST", () => {
  let component: PlaylistHeaderComponent;
  let fixture: ComponentFixture<PlaylistHeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PlaylistHeaderComponent, SecondToTimePipe],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {

    fixture = TestBed.createComponent(PlaylistHeaderComponent);
    component = fixture.componentInstance;
    component.playlist = {
      id: 1,
      picture_medium: "picture1",
      title: "title1",
      duration: 100,
      creator: "creator1"
    };
    fixture.detectChanges();
  });

  it("should create PlaylistHeaderComponent", () => {


    expect(component).toBeTruthy();
  });
});
