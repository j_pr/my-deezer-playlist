import { Component, Input, isDevMode, OnInit } from "@angular/core";
import { Track } from "../../models/track";

@Component({
  selector: "app-playlist-tracks",
  templateUrl: "./playlist-tracks.component.html",
  styleUrls: ["./playlist-tracks.component.css"]
})
export class PlaylistTracksComponent implements OnInit {
  @Input() tracks: Track[];

  constructor() {}

  ngOnInit() {
    if (isDevMode()) {
      console.log("initialisation component PlaylistTracksComponent : ", this.tracks);
    }
  }

}
