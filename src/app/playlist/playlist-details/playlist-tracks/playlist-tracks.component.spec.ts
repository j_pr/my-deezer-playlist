import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { PlaylistTracksComponent } from "./playlist-tracks.component";
import { NO_ERRORS_SCHEMA } from "@angular/core";
import { SecondToTimePipe } from "../../../utilities/TimePipe";
import { ScrollingModule } from "@angular/cdk/scrolling";

describe("PlaylistTracksComponent TEST", () => {
  let component: PlaylistTracksComponent;
  let fixture: ComponentFixture<PlaylistTracksComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PlaylistTracksComponent, SecondToTimePipe],
      imports: [ScrollingModule],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlaylistTracksComponent);
    component = fixture.componentInstance;
    component.tracks = [
      {
        id: 1,
        title: "title1",
        artist: "artist1",
        duration: "100"
      },
      {
        id: 2,
        title: "title2",
        artist: "artist2",
        duration: "200"
      }
    ];
    fixture.detectChanges();
  });

  it("should create PlaylistTracksComponent", () => {
    expect(component).toBeTruthy();
  });
});
