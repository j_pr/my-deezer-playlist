import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { PlaylistDetailsComponent } from "./playlist-details.component";
import { NO_ERRORS_SCHEMA } from "@angular/core";
import { RouterTestingModule } from "@angular/router/testing";
import { HttpClientModule } from "@angular/common/http";
import { DeezerServices } from "../services/deezer-services.service";
import { of } from "rxjs";
import { SecondToTimePipe } from "../../utilities/TimePipe";

describe("PlaylistDetailsComponent TEST", () => {
  let deezerService: DeezerServices;
  let component: PlaylistDetailsComponent;
  let fixture: ComponentFixture<PlaylistDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PlaylistDetailsComponent, SecondToTimePipe],
      imports: [RouterTestingModule, HttpClientModule],
      providers: [DeezerServices],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
    deezerService = TestBed.get(DeezerServices);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlaylistDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create PlaylistDetailsComponent", () => {
    // mock service getPlaylistInformation
    const header = {
      id: "1",
      picture_medium: "picture1",
      title: "title1",
      duration: "100",
      creator: "creator1"
    };
    spyOn(deezerService, "getPlaylistInformation").and.returnValue(of(header));

    // mock service getTracksInformation
    const tracks = [
      {
        id: "1",
        title: "title1",
        artist: "artist1",
        duration: "100"
      },
      {
        id: "2",
        title: "title2",
        artist: "artist2",
        duration: "200"
      }
    ];
    spyOn(deezerService, "getTracksInformation").and.returnValue(of(tracks));

    expect(component).toBeTruthy();
  });
});
