import { Routes, RouterModule } from "@angular/router";
import { PageNotFoundComponent } from "./utilities/not-found.component";
import { PreloadSelectedModuleStrategy } from "./preload-selected-module-strategy";
import { PlaylistComponent } from "./playlist/playlist.component";
import { PlaylistDetailsComponent } from "./playlist/playlist-details/playlist-details.component";

export const appRoutes: Routes = [
  {
    path: "",
    redirectTo: "/playlist",
    pathMatch: "full"
  },
  {
    path: "playlist",
    component: PlaylistComponent,
    data: { preload: true }
  },
  {
    path: "playlist/:id/details",
    component: PlaylistDetailsComponent,
    data: { preload: false }
  },
  {
    path: "**",
    component: PageNotFoundComponent
  }
];

export let AppRoutesModule = RouterModule.forRoot(appRoutes, {
  enableTracing: false,
  preloadingStrategy: PreloadSelectedModuleStrategy
});
