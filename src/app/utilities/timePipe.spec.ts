import { SecondToTimePipe } from "./TimePipe";

describe('TimePipe test', () => {
  const pipe = new SecondToTimePipe();
  it('create an instance of TimePipe', () => {
    expect(pipe).toBeTruthy();
  });

  it('transform(3661) should tranform to time 01:01:01', () => {
    expect(pipe.transform(3661) === '01:01:01').toBeTruthy();
  });

  it('transform(36610) should tranform to time 10:10:10', () => {
    expect(pipe.transform(36610) === '10:10:10').toBeTruthy();
  });

});
