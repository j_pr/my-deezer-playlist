import { TestBed } from "@angular/core/testing";

import { MyLoadingService } from "./my-loading.service";

describe("MyLoadingService TEST", () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it("should be created MyLoadingService", () => {
    const service: MyLoadingService = TestBed.get(
      MyLoadingService
    );
    expect(service).toBeTruthy();
  });

  it("loadingOn() should set to true ", () => {
    const service: MyLoadingService = TestBed.get(
      MyLoadingService
    );
    service.loading = false;
    service.loadingOn();
    expect(service.loading).toBeTruthy();
  });

  it("loadingOff() should set to false ", () => {
    const service: MyLoadingService = TestBed.get(
      MyLoadingService
    );
    service.loading = true;
    service.loadingOff();
    expect(service.loading).not.toBeTruthy();
  });

  it("getLoading() should return loading ", () => {
    const service: MyLoadingService = TestBed.get(
      MyLoadingService
    );
    service.loading = true;
    let test = service.getLoading();
    expect(test).toBeTruthy();
  });

});
