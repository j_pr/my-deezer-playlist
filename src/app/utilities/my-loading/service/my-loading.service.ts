import {Injectable, isDevMode} from "@angular/core";

@Injectable({
  providedIn: "root"
})
export class MyLoadingService {
  loading: boolean;
  constructor() {}

  loadingOn() {
    if(isDevMode()){
      console.log("loading ON")
    }
    this.loading = true;
  }

  loadingOff() {
    if(isDevMode()){
      console.log("loading OFF")
    }
    this.loading = false;
  }

  getLoading(): boolean {
    if (this.loading === null || this.loading === undefined) {
      this.loading = false;
      return this.loading;
    } else {
      return this.loading;
    }
  }
}
