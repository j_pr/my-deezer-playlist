import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { MyLoadingComponent } from "./my-loading.component";
import { NO_ERRORS_SCHEMA } from "@angular/core";

describe("MyLoadingComponent TEST", () => {
  let component: MyLoadingComponent;
  let fixture: ComponentFixture<MyLoadingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [MyLoadingComponent],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyLoadingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create MyLoadingComponent", () => {
    expect(component).toBeTruthy();
  });
});
