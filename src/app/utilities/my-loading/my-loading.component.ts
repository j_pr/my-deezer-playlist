import {Component, isDevMode, OnInit} from "@angular/core";
import { MyLoadingService } from "./service/my-loading.service";

@Component({
  selector: "app-my-loading",
  templateUrl: "./my-loading.component.html",
  styleUrls: ["./my-loading.component.css"]
})
export class MyLoadingComponent implements OnInit {
  constructor(private load: MyLoadingService) {}

  ngOnInit() {
    if(isDevMode()){
      console.log("initialisation component MyLoadingComponent")
    }
  }

  getLoading(): boolean {
    return this.load.getLoading();
  }
}
