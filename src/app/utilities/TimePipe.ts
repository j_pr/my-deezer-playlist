import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'secondToTime'
})
export class SecondToTimePipe implements PipeTransform {

  transform(value: number): string {
    let heure;
    let min;
    let sec;
    const sec_num = value;
    const hours   = Math.floor(sec_num / 3600);
    const minutes = Math.floor((sec_num - (hours * 3600)) / 60);
    const seconds = sec_num - (hours * 3600) - (minutes * 60);

    if (hours   < 10) {heure = "0"+hours;} else {heure = hours}
    if (minutes < 10) {min = "0"+minutes;} else {min = minutes}
    if (seconds < 10) {sec = "0"+seconds;} else {sec = seconds}

    return heure+':'+min+':'+sec;
  }
}
