import {Component} from '@angular/core';

@Component({
  template: `
    <div>
      <button mat-raised-button routerLink="/playlist" color="primary">retourner à l'accueil</button> 
    </div>
    <img src="../../assets/img/404.jpg" alt="404" />`,
})
export class PageNotFoundComponent {
}

