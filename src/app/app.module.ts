import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { AppComponent } from "./app.component";
import { PlaylistComponent } from "./playlist/playlist.component";
import { PlaylistDetailsComponent } from "./playlist/playlist-details/playlist-details.component";
import { PlaylistHeaderComponent } from "./playlist/playlist-details/playlist-header/playlist-header.component";
import { PlaylistTracksComponent } from "./playlist/playlist-details/playlist-tracks/playlist-tracks.component";
import { MyLoadingComponent } from "./utilities/my-loading/my-loading.component";
import { MyLoadingService } from "./utilities/my-loading/service/my-loading.service";
import { NgxLoadingModule } from "ngx-loading";
import { AppRoutesModule } from "./app-routing.module";
import { PageNotFoundComponent } from "./utilities/not-found.component";
import { PreloadSelectedModuleStrategy } from "./preload-selected-module-strategy";
import { HttpClientModule } from "@angular/common/http";
import {
  MatButtonModule,
  MatButtonToggleModule,
  MatExpansionModule,
  MatFormFieldModule,
  MatInputModule,
  MatPaginatorModule,
  MatSortModule,
  MatTableModule,
  MatToolbarModule
} from "@angular/material";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { SecondToTimePipe } from "./utilities/TimePipe";
import { ScrollingModule } from "@angular/cdk/scrolling";

@NgModule({
  declarations: [
    AppComponent,
    PageNotFoundComponent,
    PlaylistComponent,
    PlaylistDetailsComponent,
    PlaylistHeaderComponent,
    PlaylistTracksComponent,
    MyLoadingComponent,
    SecondToTimePipe
  ],
  imports: [
    BrowserModule,
    AppRoutesModule,
    HttpClientModule,
    NgxLoadingModule.forRoot({}),
    MatTableModule,
    MatSortModule,
    MatFormFieldModule,
    MatInputModule,
    MatPaginatorModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatToolbarModule,
    MatExpansionModule,
    ScrollingModule
  ],
  providers: [PreloadSelectedModuleStrategy, MyLoadingService],
  bootstrap: [AppComponent]
})
export class AppModule {}
