export const environment = {
  production: true,
  rest: {
    deezer: {
      urlUserPlaylist: 'http://api.deezer.com/user/:userId/playlists',
      urlPlaylistInformation: 'http://api.deezer.com/playlist/:playlistId',
      urlTracksInformation: 'http://api.deezer.com/playlist/:playlistId/tracks',
    },
    corsProxy: 'https://cors-anywhere.herokuapp.com'
  }
};
